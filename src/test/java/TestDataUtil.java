/*
 *    Copyright (c) 2019. Threadneedle Software Limited
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class TestDataUtil {

    static final Logger LOGGER = LoggerFactory.getLogger(TestDataUtil.class);

    public static void main(String[] args) throws Exception {

        LOGGER.info("Current path {}", Paths.get(".").toAbsolutePath());

        List<String> data = Files.readAllLines(Paths.get("src/main/resources/test_data_small.csv"));

        LOGGER.info("File row count {}", data.size());

        List<String> headers = data.stream()
                .filter(row -> row.split(",")[0].equalsIgnoreCase("Incident_Id"))
                .map(row -> row.split(","))
                .flatMap(row -> Arrays.stream(row))
                .collect(Collectors.toList());

        LOGGER.info("Headers: {}", headers);

        List<String[]> rows = data.stream()
                .filter(row -> !row.split(",")[0].equalsIgnoreCase("Incident_Id"))
                .map(row -> row.split(","))
                .collect(Collectors.toList());

        LOGGER.info("Data row count {}", rows.size());

        List<String> postcodes = rows.stream()
                .map(fields -> fields[5])
                .distinct()
                .collect(Collectors.toList());

        LOGGER.info("Postcodes count {}", postcodes.size());

        Path output = Paths.get("src/main/resources/test_data_small.csv");
        Files.write(output, String.join(",", headers).concat("\n").getBytes(), StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE);

        Random random = new Random();
        Supplier<String> postcodeSupplier = () -> postcodes.get(random.nextInt(postcodes.size()));

        final AtomicInteger i = new AtomicInteger(0);

        rows.stream()
                .forEach(strings -> {
                    strings[0] = String.valueOf(random.nextInt(Integer.MAX_VALUE));
                    strings[1] = String.valueOf(random.nextInt(Integer.MAX_VALUE));
                    strings[2] = String.valueOf(random.nextInt(Integer.MAX_VALUE));
                    strings[5] = postcodeSupplier.get();
                    try {
                        String line;
                        if (i.incrementAndGet() < rows.size()) {
                            line = String.join(",", strings).concat("\n");
                        } else {
                            line = String.join(",", strings);
                        }
                        Files.write(output, line.getBytes(), StandardOpenOption.APPEND);
                    } catch (IOException e) {
                        LOGGER.error(e.getMessage(), e);
                    }
                });

    }
}


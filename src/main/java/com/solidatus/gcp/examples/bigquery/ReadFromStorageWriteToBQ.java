/*
 *    Copyright (c) 2019. Threadneedle Software Limited
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.solidatus.gcp.examples.bigquery;

import com.google.api.services.bigquery.model.TableRow;
import com.ttl.gcp.dataflow.BigQuerySchema;
import com.ttl.gcp.dataflow.solidatus.LineageMapper;
import com.ttl.gcp.dataflow.solidatus.LineageMappingBuilder;
import com.ttl.gcp.dataflow.solidatus.LineageOptions;
import com.ttl.gcp.dataflow.solidatus.LineagePipeline;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
import org.apache.beam.sdk.options.Default;
import org.apache.beam.sdk.options.Description;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.ParDo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.UUID;

/**
 * Example Apache Beam job to read a file from Google Cloud Storage and write it to Big Query.
 * <p>
 * This job defines data lineage mappings and visualises the lineage in Solidatus.
 * </p>
 * <p>
 * When this job is executed the lineage is published to Solidatus when the pipeline is constructed, then the
 * pipeline is executed on the specified runner
 * </p>
 * To run the example refer to the ReadMe.md
 */
public class ReadFromStorageWriteToBQ implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReadFromStorageWriteToBQ.class);

    public static void main(String[] args) {
        ReadFromStorageWriteToBQ beamJob = new ReadFromStorageWriteToBQ();
        LineagePipeline pipeline = beamJob.buildPipeline(args);
        pipeline.run();
    }

    public LineagePipeline buildPipeline(String[] args) {

        ExampleOptions options = PipelineOptionsFactory.fromArgs(args)
                .withValidation()
                .as(ExampleOptions.class);

        LineagePipeline pipeline = LineagePipeline.create(options);

/*        LineageMapper<String, String> readFromStorage = new LineageMappingBuilder<String, String>()
                .build("Read from Storage");*/

        UUID parentId = UUID.randomUUID();

        LineageMapper<String[], TableRow> stringToTableRow = new LineageMappingBuilder<String[], TableRow>()
                .of("function_1", "uuid", (strings, row) -> row.set("uuid", UUID.randomUUID().toString()))
                .of("function_2", "parent_id", (strings, row) -> row.set("parent_id", parentId.toString()))
                .of("element[0]", "Incident_Id", (strings, row) -> row.set("Incident_Id", strings[0]))
                .of("element[1]", "Property_Id", (strings, row) -> row.set("Property_Id", strings[1]))
                .of("element[2]", "GIS_ID", (strings, row) -> row.set("GIS_ID", strings[2]))
                .of("element[3]", "Incident_Date", (strings, row) -> row.set("Incident_Date", strings[3]))
                .of("element[4]", "Catchment", (strings, row) -> row.set("Catchment", strings[4]))
                .of("element[5]", "Postcode", (strings, row) -> row.set("Postcode", strings[5]))
                .of("element[6]", "Sewer_Type", (strings, row) -> row.set("Sewer_Type", strings[6]))
                .of("element[7]", "Sewer_Status", (strings, row) -> row.set("Sewer_Status", strings[7]))
                .of("element[8]", "Fault", (strings, row) -> row.set("Fault", strings[8]))
                .of("element[9]", "Cause1", (strings, row) -> row.set("Cause1", strings[9]))
                .of("element[10]", "Cause2", (strings, row) -> row.set("Cause2", strings[10]))
                .of("element[11]", "Sevre_Weather", (strings, row) -> row.set("Sevre_Weather", Integer.valueOf(strings[11])))
                .of("element[12]", "Occupied_Building", (strings, row) -> row.set("Occupied_Building", Integer.valueOf(strings[12])))
                .of("element[13]", "Inhabited_Basement", (strings, row) -> row.set("Inhabited_Basement", Integer.valueOf(strings[13])))
                .of("element[14]", "Integral_Garage", (strings, row) -> row.set("Integral_Garage", Integer.valueOf(strings[14])))
                .of("element[15]", "Other_Building", (strings, row) -> row.set("Other_Building", Integer.valueOf(strings[15])))
                .of("element[16]", "Curtilage_Garden", (strings, row) -> row.set("Curtilage_Garden", Integer.valueOf(strings[16])))
                .of("element[17]", "Public_Space", (strings, row) -> row.set("Public_Space", Integer.valueOf(strings[17])))
                .of("element[18]", "Highway", (strings, row) -> row.set("Highway", Integer.valueOf(strings[18])))
                .of("element[19]", "Agricultural", (strings, row) -> row.set("Agricultural", Integer.valueOf(strings[19])))
                .of("element[20]", "Detached_Garage", (strings, row) -> row.set("Detached_Garage", Integer.valueOf(strings[20])))
                .of("element[21]", "Uninhab_Cellar", (strings, row) -> row.set("Uninhab_Cellar", Integer.valueOf(strings[21])))
                .of("element[22]", "Equipment_Failure_Flag", (strings, row) -> row.set("Equipment_Failure_Flag", Integer.valueOf(strings[22])))
                .of("element[23]", "LAT", (strings, row) -> row.set("LAT", Float.valueOf(strings[23])))
                .of("element[24]", "LON", (strings, row) -> row.set("LON", Float.valueOf(strings[24])))
                .build("Convert to TableRow");

        pipeline
                .apply("Read from Storage", TextIO.read().from(options.getInputFile()))
                .apply("Convert to TableRow", ParDo.of(new DoFn<String, TableRow>() {
                    @ProcessElement
                    public void process(ProcessContext context) {
                        TableRow row = new TableRow();
                        String[] fields = context.element().split(",");
                        //Skip the CSV header row
                        if (fields[0].equalsIgnoreCase("Incident_Id")) {
                            return;
                        }
                        stringToTableRow.apply(fields, row);
                        context.output(row);
                    }
                }))
                .apply("Write to BigQuery", BigQueryIO.writeTableRows()
                        .to(options.getBqTable())
                        .withSchema(BigQuerySchema.schema)
                        .withCreateDisposition(BigQueryIO.Write.CreateDisposition.CREATE_IF_NEEDED)
                        .withWriteDisposition(BigQueryIO.Write.WriteDisposition.WRITE_TRUNCATE)
                );


        return pipeline;
    }

    interface ExampleOptions extends LineageOptions {

        @Description("Input file")
        @Default.String("src/main/resources/test_data_small.csv")
        String getInputFile();

        void setInputFile(String value);

        @Description("Destination table [projectId:datasetId.tableId]")
        @Default.String("ttl-reg-ods:examples.beam_demo")
        String getBqTable();

        void setBqTable(String table);

        @Description("Output file")
        String getOutput();
        void setOutput(String output);

    }
}

/*
 *    Copyright (c) 2019. Threadneedle Software Limited
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.solidatus.gcp.examples.pubsub;

import com.google.api.gax.core.CredentialsProvider;
import com.google.api.gax.core.NoCredentialsProvider;
import com.google.api.gax.grpc.GrpcTransportChannel;
import com.google.api.gax.rpc.AlreadyExistsException;
import com.google.api.gax.rpc.FixedTransportChannelProvider;
import com.google.api.gax.rpc.TransportChannelProvider;
import com.google.cloud.pubsub.v1.TopicAdminClient;
import com.google.cloud.pubsub.v1.TopicAdminSettings;
import com.google.pubsub.v1.ProjectTopicName;
import com.google.pubsub.v1.Topic;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Optional;

/**
 * Utility helper class for connecting to and setting up topics and subscriptions
 * for the local Google PubSub Emulator
 */
public class PubSubHelper {

    private static final Logger LOGGER  = LoggerFactory.getLogger(PubSubHelper.class);

    public static Topic setUpPubSub(String projectId, String topicId) throws IOException {
        String hostport = Optional.ofNullable(System.getenv("PUBSUB_EMULATOR_HOST")).orElse("localhost:8085");
        ManagedChannel channel = ManagedChannelBuilder.forTarget(hostport).usePlaintext(true).build();
        try {
            TransportChannelProvider channelProvider =
                    FixedTransportChannelProvider.create(GrpcTransportChannel.create(channel));
            CredentialsProvider credentialsProvider = NoCredentialsProvider.create();

            // Set the channel and credentials provider when creating a `TopicAdminClient`.
            // Similarly for SubscriptionAdminClient
            TopicAdminClient topicClient =
                    TopicAdminClient.create(
                            TopicAdminSettings
                                    .newBuilder()
                                    .setTransportChannelProvider(channelProvider)
                                    .setCredentialsProvider(credentialsProvider)
                                    .build());

            try {
                return topicClient.createTopic(ProjectTopicName.of(projectId, topicId));
            } catch (AlreadyExistsException ex) {
                return topicClient.getTopic(ProjectTopicName.of(projectId, topicId));
            }
            // Set the channel and credentials provider when creating a `Publisher`.
            // Similarly for Subscriber
            //Publisher publisher =
            //        Publisher.newBuilder(topicName)
            //                .setChannelProvider(channelProvider)
            //                .setCredentialsProvider(credentialsProvider)
            //                .build();
        } finally {
            channel.shutdown();
        }
    }
}

/*
 *    Copyright (c) 2019. Threadneedle Software Limited
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.solidatus.gcp.examples.bigquery;

import com.google.api.services.bigquery.model.TableRow;
import com.google.cloud.Timestamp;
import com.ttl.gcp.dataflow.BigQuerySchema;
import com.ttl.gcp.dataflow.Meta;
import com.ttl.gcp.dataflow.RawSIRF;
import com.ttl.gcp.dataflow.solidatus.LineageMapper;
import com.ttl.gcp.dataflow.solidatus.LineageMappingBuilder;
import com.ttl.gcp.dataflow.solidatus.LineageOptions;
import com.ttl.gcp.dataflow.solidatus.LineagePipeline;
import org.apache.beam.sdk.coders.AvroCoder;
import org.apache.beam.sdk.extensions.gcp.options.GcpOptions;
import org.apache.beam.sdk.io.FileSystems;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
import org.apache.beam.sdk.io.gcp.bigquery.SchemaAndRecord;
import org.apache.beam.sdk.options.Default;
import org.apache.beam.sdk.options.Description;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.Create;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.transforms.View;
import org.apache.beam.sdk.values.PCollection;
import org.apache.beam.sdk.values.PCollectionView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.UUID;

public class ProcessRawSIRFDataV2 implements Serializable {

    private static Logger LOGGER = LoggerFactory.getLogger(ProcessRawSIRFDataV2.class);

    public static void main(String[] args) {
        ProcessRawSIRFDataV2 beamJob = new ProcessRawSIRFDataV2();
        LineagePipeline pipeline = beamJob.buildPipeline(args);
        pipeline.publishLineage();
    }

    private LineageMapper<RawSIRF, TableRow> serialMetaToTableRowMapper = new LineageMappingBuilder<RawSIRF, TableRow>()
            .with("uuid", "uuid")
            .with("parentId", "parent_id")
            .of("type", "type", Meta.Type.SERIAL::name)
            .of("origin","origin", () -> "Water Utility plc" )
            .of("created","created", () -> Timestamp.now().toString())
            .of("modified","modified", () -> Timestamp.now().toString())
            .build("Write SERIAL meta");

    private LineageMapper<RawSIRF, TableRow> writeSerialObjects = new LineageMappingBuilder<RawSIRF, TableRow>()
            .with("uuid", "uuid")
            .with("parentId", "parent_Id")
            .with("propertyId", "Property_Id")
            .with("GIS_ID", "GIS_ID")
            .with("inidentDate", "Incident_Date")
            .with("catchment", "Catchment")
            .with("postcode", "Postcode")
            .with("sewerType", "Sewer_Type")
            .with("sewerStatus", "Sewer_Status")
            .with("fault", "Fault")
            .with("cause1", "Cause1")
            .with("cause2", "Cause2")
            .with("sevreWeather", "Sevre_Weather")
            .with("occupiedBuilding", "Occupied_Building")
            .with("inhabitedBasement", "Inhabited_Basement")
            .with("integralGarage", "Integral_Garage")
            .with("otherBuilding", "Other_Building")
            .with("curtilageGarden", "Curtilage_Garden")
            .with("publicSpace", "Public_Space")
            .with("highway", "Highway")
            .with("agricultural", "Agricultural")
            .with("detachedGarage", "Detached_Garage")
            .with("uninhabCellar", "Uninhab_Cellar")
            .with("equipmentFailureFlag", "Equipment_Failure_Flag")
            .with("latitude", "LAT")
            .with("longtitude", "LON")
            .build("Write SERIAL objects");

    private LineageMapper<RawSIRF, TableRow> writeStndObjects = new LineageMappingBuilder<RawSIRF, TableRow>()
            .with("uuid", "uuid")
            .with("parentId", "parent_Id")
            .with("propertyId", "Property_Id")
            .with("GIS_ID", "GIS_ID")
            .with("inidentDate", "Incident_Date")
            .with("catchment", "Catchment")
            .with("postcode", "Postcode")
            .with("sewerType", "Sewer_Type")
            .with("sewerStatus", "Sewer_Status")
            .with("fault", "Fault")
            .with("cause1", "Cause1")
            .with("cause2", "Cause2")
            .with("sevreWeather", "Sevre_Weather")
            .with("occupiedBuilding", "Occupied_Building")
            .with("inhabitedBasement", "Inhabited_Basement")
            .with("integralGarage", "Integral_Garage")
            .with("otherBuilding", "Other_Building")
            .with("curtilageGarden", "Curtilage_Garden")
            .with("publicSpace", "Public_Space")
            .with("highway", "Highway")
            .with("agricultural", "Agricultural")
            .with("detachedGarage", "Detached_Garage")
            .with("uninhabCellar", "Uninhab_Cellar")
            .with("equipmentFailureFlag", "Equipment_Failure_Flag")
            .with("latitude", "LAT")
            .with("longtitude", "LON")
            .build("Write STND objects");

    private LineageMapper<Meta, TableRow> metaObjectMapper = new LineageMappingBuilder<Meta, TableRow>()
            .with("uuid", "uuid")
            .with("parentId", "parent_id")
            .with("type", "type")
            .with("origin", "origin")
            .with("created", "created")
            .with("modified", "modified")
            .build("Write Parent to Meta");

    private LineageMapper<String[], RawSIRF> csvToRawSirfMapper = new LineageMappingBuilder<String[], RawSIRF>()
            //.of("function1", "uuid", strings -> UUID.randomUUID().toString())
            //.of("fucntion2", "parentId", strings -> "parentId") //This is a sideInput to the pipeline and is overridden in the pipeline
            .of("element[0]", "incidentId", strings -> strings[0])
            .of("element[1]", "propertyId", strings -> strings[1])
            .of("element[2]", "GIS_ID", strings -> strings[2])
            .of("element[3]", "inidentDate", strings -> strings[3])
            .of("element[4]", "catchment", strings -> strings[4])
            .of("element[5]", "postcode",strings -> strings[5])
            .of("element[6]", "sewerType",strings -> strings[6])
            .of("element[7]", "sewerStatus", strings -> strings[7])
            .of("element[8]", "fault", strings -> strings[8])
            .of("element[9]", "cause1", strings -> strings[9])
            .of("element[10]", "cause2", strings-> strings[10])
            .of("element[11]", "sevreWeather", strings -> Integer.valueOf(strings[11]))
            .of("element[12]", "occupiedBuilding", strings -> Integer.valueOf(strings[12]))
            .of("element[13]", "inhabitedBasement", strings -> Integer.valueOf(strings[13]))
            .of("element[14]", "integralGarage", strings -> Integer.valueOf(strings[14]))
            .of("element[15]", "otherBuilding", strings -> Integer.valueOf(strings[15]))
            .of("element[16]", "curtilageGarden", strings -> Integer.valueOf(strings[16]))
            .of("element[17]", "publicSpace", strings -> Integer.valueOf(strings[17]))
            .of("element[18]", "highway", strings -> Integer.valueOf(strings[18]))
            .of("element[19]", "agricultural", strings -> Integer.valueOf(strings[19]))
            .of("element[20]", "detachedGarage", strings -> Integer.valueOf(strings[20]))
            .of("element[21]", "uninhabCellar", strings -> Integer.valueOf(strings[21]))
            .of("element[22]", "equipmentFailureFlag", strings -> Integer.valueOf(strings[22]))
            .of("element[23]", "latitude", strings-> Float.valueOf(strings[23]))
            .of("element[24]", "longtitude", strings -> Float.valueOf(strings[24]))
            .build("Extract Rows");

    private LineageMapper<SchemaAndRecord, RawSIRF> readStdTableRowToRawSirfMapper = new LineageMappingBuilder<SchemaAndRecord, RawSIRF>()
            .of("uuid","uuid", input -> input.getRecord().get("uuid"))
            .of("parent_id", "parentId", input -> input.getRecord().get("parent_id"))
            .build("Read SERIALIZED data");

    private LineageMapper<RawSIRF, TableRow> writeStdMetaToTableRowMapper = new LineageMappingBuilder<RawSIRF, TableRow>()
            .with("uuid", "uuid")
            .with("parentId", "parent_id")
            .of("type", "type", () -> Meta.Type.STANDARDISED.name())
            .of("origin", "origin", () ->"Water Utility plc")
            .of("created","created",() -> Timestamp.now().toString())
            .of("modified", "modified", () -> Timestamp.now().toString())
            .build("Write STND meta");

    private LineageMapper<RawSIRF, RawSIRF> parentMetaSingleton = new LineageMappingBuilder<RawSIRF, RawSIRF>()
            .with("uuid", "uuid")
            .with("parentId", "parentId")
            .with("type", "type")
            .with("origin", "origin")
            .with("created", "created")
            .with("modified", "modified")
            .build("Create parent Meta singleton");

    @SuppressWarnings("Duplicates")
    public LineagePipeline buildPipeline(String[] args) {

        FileSystems.setDefaultPipelineOptions(PipelineOptionsFactory.create());

        // Create a PipelineOptions object. This object lets us set various execution
        // options for our pipeline, such as the runner you wish to use. This example
        // will run with the DirectRunner by default, based on the class path configured
        // in its dependencies.
        ExampleOptions options = PipelineOptionsFactory.fromArgs(args)
                .withValidation()
                .as(ExampleOptions.class);

        // Create the Pipeline object with the options we defined above.
        LineagePipeline p = LineagePipeline.create(options);

        String PROJECT_ID = options.getProject();
        String META_DATSET = PROJECT_ID + ":" + options.getMetaDataset();
        String RAW_DATASET = PROJECT_ID + ":" + options.getRawDataset();
        String SERIAL_DATASET = PROJECT_ID + ":" + options.getSerialDataset();
        String STANDARD_DATASET = PROJECT_ID + ":" + options.getStandardDataset();


        Meta parentMeta = new Meta(UUID.randomUUID().toString(),
                "NA",
                Meta.Type.RAW_FILE,
                options.getInputFile(),
                Timestamp.now().toString(),
                "NA");

        // Apply the pipeline's transforms.
        PCollection<Meta> parentMetaPCollection = p.apply("Create parent Meta", Create.of(parentMeta));
        PCollectionView<Meta> parentMetaPCollectionView = parentMetaPCollection.apply("Create parent Meta singleton", View.asSingleton());

        parentMetaPCollection.apply("Write Parent to Meta", BigQueryIO.<Meta>write()
                .to(META_DATSET + "." + "control_object")
                .withSchema(BigQuerySchema.metaSchema)
                .withFormatFunction(this::applyWriteParentToMeta)
                .withCreateDisposition(BigQueryIO.Write.CreateDisposition.CREATE_IF_NEEDED)
                .withWriteDisposition(BigQueryIO.Write.WriteDisposition.WRITE_TRUNCATE)
        );

        // Read the source data file from Google Cloud Storage
        PCollection<RawSIRF> sourceCollection = p
                .apply("Read Source", TextIO.read().from(((ExampleOptions) options).getInputFile()))
                .apply("Extract Rows", ParDo.of(new DoFn<String, RawSIRF>() {
                                                    @ProcessElement
                                                    public void processElement(ProcessContext c) {
                    String[] fields = c.element().split(",");

                    //Skip the CSV header row
                    if (fields[0].equalsIgnoreCase("Incident_Id")) {
                        return;
                    }
                    RawSIRF raw = new RawSIRF();
                    csvToRawSirfMapper.apply(fields, raw);
                                                        raw.setUuid(UUID.randomUUID().toString());
                    //Override the defalut value of parentId with the sideInput
                    raw.setParentId(c.sideInput(parentMetaPCollectionView).getParentId());
                    c.output(raw);
                    }
                })
                .withSideInputs(parentMetaPCollectionView));

        //Write input data to RAW table
        sourceCollection.apply("Write SERIAL objects", BigQueryIO.<RawSIRF>write()
                .to(SERIAL_DATASET + "." + "sirf")
                .withFormatFunction(input -> {
                    TableRow row = new TableRow();
                    writeSerialObjects.apply(input, row);
                    return row;
                })
                .withSchema(BigQuerySchema.schema)
                .withWriteDisposition(BigQueryIO.Write.WriteDisposition.WRITE_TRUNCATE)
                .withCreateDisposition(BigQueryIO.Write.CreateDisposition.CREATE_NEVER)
        );


        //Write Control data to Meta table
        sourceCollection.apply("Write SERIAL meta", BigQueryIO.<RawSIRF>write()
                .to(META_DATSET + "." + "control_object")
                .withFormatFunction(input -> {
                    TableRow row = new TableRow();
                    serialMetaToTableRowMapper.apply(input, row);
                    return row;
                })
                .withSchema(BigQuerySchema.metaSchema)
                .withWriteDisposition(BigQueryIO.Write.WriteDisposition.WRITE_APPEND)
                .withCreateDisposition(BigQueryIO.Write.CreateDisposition.CREATE_NEVER)
        );

        //Write data to STANDARD table
        sourceCollection.apply("Write STND objects", BigQueryIO.<RawSIRF>write()
                .to(STANDARD_DATASET + "." + "sirf")
                .withFormatFunction(input -> {
                    TableRow row = new TableRow();
                    writeStndObjects.apply(input, row);
                    return row;
                })
                .withSchema(BigQuerySchema.schema)
                .withWriteDisposition(BigQueryIO.Write.WriteDisposition.WRITE_TRUNCATE)
                .withCreateDisposition(BigQueryIO.Write.CreateDisposition.CREATE_NEVER)
        );

        p.apply("Read SERIALIZED data",
                BigQueryIO.read(input -> {
                    RawSIRF raw = new RawSIRF();
                    readStdTableRowToRawSirfMapper.apply(input, raw);
                    return raw;
                })
                        .from(STANDARD_DATASET + "." + "sirf")
                        .withCoder(AvroCoder.of(RawSIRF.class))
        )
                .apply("Write STND meta", BigQueryIO.<RawSIRF>write()
                        .to(META_DATSET + "." + "control_object")
                        .withFormatFunction(input -> {
                            TableRow row = new TableRow();
                            writeStdMetaToTableRowMapper.apply(input, row);
                            return row;
                        })
                        .withSchema(BigQuerySchema.metaSchema)
                        .withWriteDisposition(BigQueryIO.Write.WriteDisposition.WRITE_APPEND)
                        .withCreateDisposition(BigQueryIO.Write.CreateDisposition.CREATE_NEVER)
                );

        return p;
    }

    private TableRow applyWriteParentToMeta(Meta input) {
        TableRow row = new TableRow();
        metaObjectMapper.apply(input, row);
        return row;
    }


    //GCP Managed DataflowRunner options
    interface ExampleOptions extends LineageOptions, GcpOptions {

        @Description("Input file")
        @Default.String("src/main/resources/test_data_small.csv")
        String getInputFile();
        void setInputFile(String value);

        String getOutput();
        void setOutput(String value);

        @Description("Big Query meta data dataset name")
        @Default.String("meta")
        String getMetaDataset();

        void setMetaDataset(String dataset);

        @Description("Big Query raw data dataset name")
        @Default.String("raw")
        String getRawDataset();

        void setRawDataset(String dataset);

        @Description("Big Query serialised data dataset name")
        @Default.String("json_serial")
        String getSerialDataset();

        void setSerialDataset(String dataset);

        @Description("Big Query standardised data dataset name")
        @Default.String("standard")
        String getStandardDataset();

        void setStandardDataset(String dataset);

    }
}

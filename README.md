![Solidatus Logo](docs/images/solidatus-1000x300.png)

##Solidatus Models for Apache Beam ![Build status](https://ci.solidatus.com/app/rest/builds/buildType:%28id:SolidatusGcp_GcpDataflowExamplesJavaDev%29/statusIcon)
A Java framework which enables a developer to declare attribute level data lineage for Apache Beam pipelines and visualise
the DAG using Solidatus.

##Supported I/O Transforms
* BigQueryIO - read and write data to/from Google BigQuery
* TextIO - read and write data to Google Cloud Storage
* PubSubIO - publish and subscribe to streaming data via Google PubSub
* ParDo  - custom Beam transforms
##Supported Coders
* AvroCoder
* StringUtf8Coder

#Examples

###Prerequisites
* a Google Cloud Project, for help see https://cloud.google.com
* Google Cloud SDK installed, for help see https://cloud.google.com/sdk/docs/quickstarts
* Google Cloud PubSub Emulator, for help see https://cloud.google.com/pubsub/docs/emulator
* Google Cloud Storage bucket, for help see https://cloud.google.com/storage/
* Google Big Query dataset, for help see https://cloud.google.com/bigquery/
* credentials for the project with authorisation to use:
    * Cloud Storage API
    * Big Query API
    * PubSub API (optional if using PubSub local emulator)
    * Cloud Dataflow API
* Solidatus user OAuth token, with Create Model permission, for help see 'help/api/authenticating' in your Solidatus instance help pages
* **_optional_** a Solidatus model configured with display rules 

### Getting Started 
Get the source

```git clone https://bitbucket.org/threadneedle/gcp-dataflow-examples-java.git```

or


```git clone git@bitbucket.org:threadneedle/gcp-dataflow-examples-java.git```


Run a maven build to create a bundled uber jar

```cd gcp-dataflow-examples-java```

```mvn clean install``` or ```mvn -DskipTests clean install```

The examples assume Solidatus is running locally and that you already have a Solidatus OAUTH Token stored in env variable 
`$SOLIDATUS_TOKEN`

If you are not running Solidatus locally set the env variable $SOLIDATUS_HOST to point to your instance e.g. 
`SOLIDATUS_HOST=https://demo.solidatus.com`

If you want to use a model as a template for Display Rules set the env variable $BEAM_RULES_TEMPLATE with the model name e.g.
`BEAM_RULES_TEMPLATE='Beam Rules Template'`
If you don't want to use a rules template omit `--modelTemplateName=${BEAM_RULES_TEMPLATE}` from the example command line

As some of the examples use Google Cloud make sure you have the GOOGLE_APPLICATION_CREDENTIALS env variable setup correctly.

```
export GOOGLE_APPLICATION_CREDENTIALS=<PATH_TO_GOOGLE_CREDENTIALS_JSON_FILE> 
```

For help setting up your Google gcloud env see https://cloud.google.com/docs/authentication

The examples below can make use of Google PubSub local Emulator, to start the emulator ensure it has been installed using `gcloud` command line tool, and then run:

```gcloud beta emulators pubsub start``` 

For help with the PubSub emulator see https://cloud.google.com/pubsub/docs/emulator

##PubSubExample
Simple Apache Beam job to publish a message onto Google PubSub and consume the same message writing it to the console. 
This example uses the PubSub Emulator for local execution.

|option|description|
|----|----|
|--host| the Solidatus host|
|--token| a valid Solidatus token|
|--modelName| name of the Solidatus model to create or update|
|--modelTemplateName| name of the Solidatus model to use as a display rules template|
|--runner| the Beam runner for the job `DirectRunner` for local execution or `DataflowRunner` to upload and execute the job on Google Dataflow|
|--streaming | tells the runner this is a Streaming job|

To run the pipeline on local DirectRunner:

```
java -cp ./target/gcp-dataflow-examples-java-bundled-1.0-SNAPSHOT.jar com.solidatus.gcp.examples.pubsub.PubSubExample \
--host=${SOLIDATUS_HOST} \
--token=${SOLIDATUS_TOKEN} \
--modelName="Simple PubSub Example" \
--modelTemplateName=${BEAM_RULES_TEMPLATE} \
--runner=DirectRunner \
--streaming 
```

**Note:** As this is a streaming job, you need to CTRL-C to exit the job if running the dataflow pipepline

##ReadFromStorageWriteToBQ
Simple Apache Beam job to read a file from Google Cloud Storage and write the contents row by row to a Big Query table.

In this example the job configuration does not read the input file from Google Cloud Storage (GCS).  To make the job read the input file from
GCS simply upload or copy the file to a GCS bucket in your project and change the `--inputFile` option to reference it 
e.g. `--inputFile=gs://my-bucket/test_data_small.csv`

|option|description|
|---|---|
|--host| the Solidatus host|
|--token| a valid Solidatus token|
|--modelName| name of the Solidatus model to create or update|
|--modelTemplateName| name of the Solidatus model to use as a display rules template|
|--runner| the Beam runner for the job `DirectRunner` for local execution or `DataflowRunner` to upload and execute the job on Google Dataflow|
|--inputFile| the input data file to write to the Bg Query table, will read from local disk or GCS if prefixed with `gs://bucket-name`|
|--bqTable| the target BigQuery table id in the form [projectId:datasetId.tableId] e.g. `myProject:myDataset.myTable`|
|--tempLocation| required GCS bucket location for temp Big Query files|

To run the pipeline on local DirectRunner:

 ```
 java -cp ./target/gcp-dataflow-examples-java-bundled-1.0-SNAPSHOT.jar com.solidatus.gcp.examples.bigquery.ReadFromStorageWriteToBQ \
 --host=${SOLIDATUS_HOST} \
 --token=${SOLIDATUS_TOKEN} \
 --modelName="Simple Read file Write BQ Example" \
 --modelTemplateName=${BEAM_RULES_TEMPLATE} \
 --runner=DirectRunner \
 --inputFile="src/main/resources/test_data_small.csv" \
 --bqTable=${BQ_TABLE} \
 --tempLocation=${GCS_TEMP_BUCKET}
 ```

##ReadFromViewPublishAndWriteToFile
Simple Apache Beam job to read from a Big Query view, publish the data to PuSub, subscribe to the data and log it to the console.
Simultaneously write the Big Query view data to a file.  This example uses the PubSub Emulator for local execution. It also uses
an Avro encoded domain object for publishing data to PubSub

|option|description|
|---|---|
|--host| the Solidatus host|
|--token| a valid Solidatus token|
|--modelName| name of the Solidatus model to create or update|
|--modelTemplateName| name of the Solidatus model to use as a display rules template|
|--runner| the Beam runner for the job `DirectRunner` for local execution or `DataflowRunner` to upload and execute the job on Google Dataflow|
|--viewName| the target BigQuery view in the form [projectId:datasetId.tableId] e.g. `myProject:myDataset.myView`|
|--topic| PubSub topic on local emulator|
|--tempLocation| required GCS bucket location for temp Big Query files|
|--output| location where output files is written to.  For GCS use `gs://bucket-name`

To run the pipeline on the local DirectRunner:

```
java -cp ./target/gcp-dataflow-examples-java-bundled-1.0-SNAPSHOT.jar com.solidatus.gcp.examples.bigquery.ReadFromViewPublishAndWriteToFile \
--host=${SOLIDATUS_HOST} \
--token=${SOLIDATUS_TOKEN} \
--modelName="BigQuery View with PubSub" \
--modelTemplateName=${BEAM_RULES_TEMPLATE} \
--viewName=${BQ_VIEW_NAME} \
--topic=${PUBSUB_TOPIC} \
--runner=DirectRunner \
--tempLocation=${GCS_TEMP_BCUKET}/tmp \
--output=${GCS_TEMP_BUCKET}/output
```

##ProcessRawSIRFDataV2
More complex Apache Beam job to perform loading of a data file into an Operational Data Store, with serialisation and standardisation steps.

This pipeline will:
* load a file from storage
* extract each row and map to a domain object
* write serialised domain object to BQ table
* write serialised domain object meta data to BQ table
* read serialised domain object from BQ table
* standardise the serialised object
* write standardised object to a BQ table
* write standardised object meta data to a BQ table

|option|description|
|---|---|
|--host| the Solidatus host|
|--token| a valid Solidatus token|
|--modelName| name of the Solidatus model to create or update|
|--modelTemplateName| name of the Solidatus model to use as a display rules template|
|--inputFile| the input data file to write to the Bg Query table, will read from local disk or GCS if prefixed with `gs://bucket-name`|
|--metaDataset| the BigQuery meta-data dataset id in the form [datasetId] e.g. `myDataset`|
|--rawDataset| the BigQuery raw data dataset id in the form [datasetId] e.g. `myDataset`|
|--serialDataset| the BigQuery serialised data dataset id in the form [datasetId] e.g. `myDataset`|
|--standardDataset| the BigQuery standardised data dataset id in the form [datasetId] e.g. `myDataset`|
|--runner| the Beam runner for the job `DirectRunner` for local execution or `DataflowRunner` to upload and execute the job on Google Dataflow|
|--tempLocation| required GCS bucket location for temp Big Query files|
|--output| location where output files is written to.  For GCS use `gs://bucket-name`|


To run the pipeline on local DirectRunner:

```
java -cp ./target/gcp-dataflow-examples-java-bundled-1.0-SNAPSHOT.jar com.solidatus.gcp.examples.bigquery.ProcessRawSIRFDataV2 \
--host=${SOLIDATUS_HOST} \
--token=${SOLIDATUS_TOKEN} \
--modelName="BQ Water Util ODS" \
--modelTemplateName=${BEAM_RULES_TEMPLATE} \
--inputFile=src/main/resources/test_data_small.csv \
--metaDataset=meta \
--rawDataset=raw \
--serialDataset=json_serial \
--standardDataset=standard \
--runner=DirectRunner \
--tempLocation=/tmp \
--output=/tmp/output
```

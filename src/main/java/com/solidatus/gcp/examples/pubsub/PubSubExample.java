/*
 *    Copyright (c) 2019. Threadneedle Software Limited
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.solidatus.gcp.examples.pubsub;

import com.ttl.gcp.dataflow.solidatus.LineageMapper;
import com.ttl.gcp.dataflow.solidatus.LineageMappingBuilder;
import com.ttl.gcp.dataflow.solidatus.LineageOptions;
import com.ttl.gcp.dataflow.solidatus.LineagePipeline;
import org.apache.beam.sdk.coders.AvroCoder;
import org.apache.beam.sdk.coders.DefaultCoder;
import org.apache.beam.sdk.io.gcp.pubsub.PubsubIO;
import org.apache.beam.sdk.io.gcp.pubsub.PubsubOptions;
import org.apache.beam.sdk.options.Default;
import org.apache.beam.sdk.options.Description;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.Create;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.ParDo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Serializable;
import java.util.StringJoiner;

/**
 * Simple Apache Beam example using GCP PubSub and PubSub Emulator with DirectRunner for local execution
 * of the Beam pipeline.
 *
 * To run the example refer to the ReadMe.md
 *
 */
public class PubSubExample implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(PubSubExample.class);

    public static void main(String[] args) throws IOException {

        PubSubExample beamJob = new PubSubExample();
        LineagePipeline pipeline = beamJob.buildPipline(args);
        pipeline.run();

    }

    public LineagePipeline buildPipline(String[] args) throws IOException {

        LineageMapper subscribeMapper = new LineageMappingBuilder<String, String>().build("Subscribe");
        LineageMapper outputtMapper = new LineageMappingBuilder<String, String>()
                .of("String", "String", m -> m.toUpperCase())
                .build("Output");
        LineageMapper generateMapper = new LineageMappingBuilder<String, String>().build("Generate");
        LineageMapper publishMapper = new LineageMappingBuilder<String, String>().build("Publish");

        ExamplesPubSubOptions options = PipelineOptionsFactory
                .fromArgs(args)
                .as(ExamplesPubSubOptions.class);

        LineagePipeline pipeline = LineagePipeline.create(options);

        PubSubHelper.setUpPubSub(options.getProject(), options.getTopic());

        pipeline
                .apply("Subscribe", PubsubIO.readStrings()
                        .fromTopic("projects/" + options.getProject() + "/topics/" + options.getTopic())
                )
                .apply("Output", ParDo.of(new DoFn<String, String>() {
                    @ProcessElement
                    public void process(ProcessContext context) {
                        LOGGER.info(context.element());
                        context.output(context.element());
                    }
                }));

        pipeline
                .apply("Generate", Create.of("This is an example String message !"))
                .apply("Publish", PubsubIO.writeStrings()
                        .to("projects/" + options.getProject() + "/topics/" + options.getTopic())
                );

        return pipeline;
    }

    @DefaultCoder(AvroCoder.class)
    static class StringMessage {

        private String message;

        public StringMessage() {
        }

        public StringMessage(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }


        @Override
        public String toString() {
            return new StringJoiner(", ", StringMessage.class.getSimpleName() + "[", "]")
                    .add("message='" + message + "'")
                    .toString();
        }
    }

    interface ExamplesPubSubOptions extends PubsubOptions, LineageOptions {

        @Override
        void setPubsubRootUrl(String value);

        @Description("PubSub topic")
        @Default.String("test")
        String getTopic();
        void setTopic(String topic);

    }

}

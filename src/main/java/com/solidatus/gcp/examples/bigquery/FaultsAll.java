/*
 *    Copyright (c) 2019. Threadneedle Software Limited
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.solidatus.gcp.examples.bigquery;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.beam.sdk.coders.AvroCoder;
import org.apache.beam.sdk.coders.DefaultCoder;

/**
 * Fault domain object - uses {@link AvroCoder}
 */
@DefaultCoder(AvroCoder.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FaultsAll {
    private String postcode = "";
    private String faults = "";
    private String lat = "";
    private String lng = "";
    private String geo = "";

    public FaultsAll() {
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getFaults() {
        return faults;
    }

    public void setFaults(String faults) {
        this.faults = faults;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getGeo() {
        return geo;
    }

    public void setGeo(String geo) {
        this.geo = geo;
    }

    @Override
    public String toString() {
        return "FaultsAll{" +
                "postcode='" + postcode + '\'' +
                ", faults='" + faults + '\'' +
                ", lat='" + lat + '\'' +
                ", lng='" + lng + '\'' +
                ", geo='" + geo + '\'' +
                '}';
    }
}

/*
 *    Copyright (c) 2019. Threadneedle Software Limited
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.solidatus.gcp.examples.bigquery;

import com.ttl.gcp.dataflow.examples.pubsub.PubSubHelper;
import com.ttl.gcp.dataflow.solidatus.LineageMapper;
import com.ttl.gcp.dataflow.solidatus.LineageMappingBuilder;
import com.ttl.gcp.dataflow.solidatus.LineageOptions;
import com.ttl.gcp.dataflow.solidatus.LineagePipeline;
import org.apache.beam.sdk.coders.AvroCoder;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
import org.apache.beam.sdk.io.gcp.bigquery.SchemaAndRecord;
import org.apache.beam.sdk.io.gcp.pubsub.PubsubIO;
import org.apache.beam.sdk.io.gcp.pubsub.PubsubOptions;
import org.apache.beam.sdk.options.Default;
import org.apache.beam.sdk.options.Description;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.values.PCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Serializable;

public class ReadFromViewPublishAndWriteToFile implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReadFromViewPublishAndWriteToFile.class);

    @SuppressWarnings("unchecked")
    public static void main(String[] args) throws IOException {

        ReadFromViewPublishAndWriteToFile beamJob = new ReadFromViewPublishAndWriteToFile();
        LineagePipeline pipeline = beamJob.buidPipeline(args);
        pipeline.run();

    }


    public LineagePipeline buidPipeline(String[] args) throws IOException {

        ExampleOptions options = PipelineOptionsFactory.fromArgs(args)
                .withValidation()
                .as(ExampleOptions.class);

        final String PROJECT_NAME = options.getProject();
        final String TOPIC_NAME = options.getTopic();
        final String PROJECT_TOPIC_NAME = "projects/" + PROJECT_NAME + "/topics/" + TOPIC_NAME;

        PubSubHelper.setUpPubSub(PROJECT_NAME, TOPIC_NAME);

        LineagePipeline pipeline = LineagePipeline.create(options);

        LineageMapper<SchemaAndRecord, FaultsAll> mapper = new LineageMappingBuilder<SchemaAndRecord, FaultsAll>()
                .of("postcode", "postcode", schemaAndRecord -> schemaAndRecord.getRecord().get("postcode"))
                .of("faults", "faults", schemaAndRecord -> schemaAndRecord.getRecord().get("faults"))
                .of("lat", "lat", schemaAndRecord -> schemaAndRecord.getRecord().get("lat"))
                .of("lng", "lng", schemaAndRecord -> schemaAndRecord.getRecord().get("lng"))
                .of("geo", "geo", schemaAndRecord -> schemaAndRecord.getRecord().get("geo"))
                .build("Read from View");

        LineageMapper<FaultsAll, StringBuilder> faultToStringMapper = new LineageMappingBuilder<FaultsAll, StringBuilder>()
                .of("postcode", "String[0]", (fault, builder) -> builder.append(fault.getPostcode()))
                .of("faults", "String[1]", (fault, builder) -> builder.append(fault.getFaults()))
                .of("lat", "String[2]", (fault, builder) -> builder.append(fault.getLat()))
                .of("lng", "String[3]", (fault, builder) -> builder.append(fault.getLng()))
                .of("geo", "String[4]", (fault, builder) -> builder.append(fault.getGeo()))
                .build("Convert");

        LineageMapper<String, String> writeToFile = new LineageMappingBuilder<String, String>().build("Write to file");

        LineageMapper<FaultsAll, FaultsAll> logMessage = new LineageMappingBuilder<FaultsAll, FaultsAll>()
                .with("postcode", "postcode")
                .with("faults", "faults")
                .with("lat", "lat")
                .with("lng", "lng")
                .with("geo", "geo")
                .build("Log Message");

        //Setup Topic subscriber
        pipeline
                .apply("Subscribe", PubsubIO.readAvros(FaultsAll.class)
                        .fromTopic(PROJECT_TOPIC_NAME))
                .apply("Log Message", ParDo.of(new DoFn<FaultsAll, FaultsAll>() {
                    @ProcessElement
                    public void process(ProcessContext context) {
                        FaultsAll fault = new FaultsAll();
                        logMessage.apply(context.element(), fault);
                        LOGGER.info(fault.toString());
                        context.output(fault);
                    }
                })).setCoder(AvroCoder.of(FaultsAll.class));


        PCollection<FaultsAll> faults = pipeline
                .apply("Read from View", BigQueryIO.read(input -> {
                    FaultsAll fault = new FaultsAll();
                    mapper.apply(input, fault);
                    return fault;
                })
                        //TODO Remove hard coded GCP project name
                        .fromQuery("SELECT * FROM `" + options.getViewName() + "` LIMIT 100")
                        .withCoder(AvroCoder.of(FaultsAll.class))
                        .usingStandardSql());

        faults.apply("Publish to PubSubExample", PubsubIO.writeAvros(FaultsAll.class)
                .to(PROJECT_TOPIC_NAME));

        faults
                .apply("Convert", ParDo.of(new DoFn<FaultsAll, String>() {
                    @ProcessElement
                    public void processElement(ProcessContext context) {
                        StringBuilder builder = new StringBuilder();
                        faultToStringMapper.apply(context.element(),builder);
                        context.output(builder.toString());
                    }
                }))
                .apply("Write to file", TextIO.write()
                        .to("/tmp/faults_all")
                        .withoutSharding());

        return pipeline;
    }

    interface ExampleOptions extends PubsubOptions, LineageOptions {

        @Description("Big Query view to select from as [projectId].[datasetId].[tableId]")
        @Default.String("ttl-reg-ods.standard.faults_all")
        String getViewName();

        void setViewName(String viewName);

        @Description("Output location")
        String getOutput();
        void setOutput(String value);

        @Description("Publishing topic name")
        @Default.String("faults_all")
        String getTopic();
        void setTopic(String topic);

        String getTempLocation();
        void setTempLocation(String location);
    }


}
